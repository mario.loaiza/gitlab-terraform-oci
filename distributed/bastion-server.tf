resource "oci_core_instance" "bastion-server" {
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "bastion-server"
  shape               = var.bastion_server_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape_bastion
    content {
      ocpus = var.bastion_server_ocpus
      memory_in_gbs = var.bastion_server_ram
    }
  }

  create_vnic_details {
    subnet_id              = local.public_bastion_subnet_id
    display_name           = "bastion-server"
    assign_public_ip       = true
    hostname_label         = "bastion-server"
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.platform_image_id
  }
 
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }
 
  freeform_tags = map(var.tag_key_name, var.tag_value)
}
