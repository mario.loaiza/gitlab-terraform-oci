locals {

  # Logic to use AD name provided by user input on ORM or to lookup for the AD name when running from CLI
  availability_domain      = (var.availability_domain_name != "" ? var.availability_domain_name : data.oci_identity_availability_domain.ad.name)

  # Logic to determine URL
  external_url             = var.external_url != "" ? var.external_url : "https://${oci_load_balancer_load_balancer.gitlab_lb.ip_address_details[0].ip_address}"
  has_http                 = length(regexall("http:", local.external_url))    > 0 ? 1 : 0
  domain                   = local.has_http == 1 ? replace(local.external_url, "http://", "") : replace(local.external_url, "https://", "")
 
  # local.use_existing_network referenced in network.tf
  use_existing_network     = var.network_strategy  == var.network_strategy_enum["USE_EXISTING_PUBLIC_VCN_SUBNET"] ? true : false

  is_flex_shape_bastion    = length(regexall("Flex", var.bastion_server_shape))    > 0 ? [1] : []
  is_flex_shape_gitlab     = length(regexall("Flex", var.gitlab_server_shape))     > 0 ? [1] : []
  is_flex_shape_redis      = length(regexall("Flex", var.redis_server_shape))      > 0 ? [1] : [] 
  is_flex_shape_postgres   = length(regexall("Flex", var.postgres_server_shape))   > 0 ? [1] : [] 
  is_flex_shape_gitaly     = length(regexall("Flex", var.gitaly_server_shape))     > 0 ? [1] : [] 
  is_flex_shape_monitoring = length(regexall("Flex", var.monitoring_server_shape)) > 0 ? [1] : [] 

  ## Customer Access Keys
  create_access_keys       = var.access_key_strategy == var.access_key_strategy_enum["CREATE_NEW_ACCESS_KEYS"] ? true : false 
  access_key               = local.create_access_keys ? oci_identity_customer_secret_key.gitlab_customer_secret_key[0].id : var.access_key
  secret_key               = local.create_access_keys ? oci_identity_customer_secret_key.gitlab_customer_secret_key[0].key : var.secret_key

  ## SMTP Credentials.
  create_smtp_credentials  = var.smtp_strategy  == var.smtp_strategy_enum["CREATE_NEW_SMTP_CREDENTIALS"] ? true : false
  smtp_username            = local.create_smtp_credentials ? oci_identity_smtp_credential.gitlab_smtp_credential[0].username : var.smtp_username
  smtp_password            = local.create_smtp_credentials ? oci_identity_smtp_credential.gitlab_smtp_credential[0].password : var.smtp_password

  platform_image_id        = var.image_ocid
 
  public_bastion_subnet_id = local.use_existing_network ? var.public_bastion_subnet_id : oci_core_subnet.bastion_public_subnet[0].id
  public_LB_subnet_id      = local.use_existing_network ? var.public_LB_subnet_id : oci_core_subnet.LB_public_subnet[0].id
  private_subnet_id        = local.use_existing_network ? var.private_subnet_id : oci_core_subnet.gitlab_private_subnet[0].id

  # This needs to be fixed for the case when user selects existing network
  private_subnet_domain_name = local.use_existing_network ? data.oci_core_subnet.gitlab_private_subnet.subnet_domain_name : oci_core_subnet.gitlab_private_subnet[0].subnet_domain_name
}
