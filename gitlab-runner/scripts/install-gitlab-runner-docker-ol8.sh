#!/bin/bash

GITLAB_SERVER_URL='${gitlab_server_url}'
GITLAB_SERVER_REGISTRATION_TOKEN='${gitlab_runner_registration_token}'
RUNNER_DOCKER_IMAGE='${gitlab_runner_docker_image}'
GITLAB_RUNNER_TAG_LIST='${gitlab_runner_tag_list}'
DOCKER_PRIVILEGED_FLAG='${gitlab_runner_docker_priv_flag}'

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce docker-ce-cli containerd.io

systemctl start docker

systemctl enable docker

mkdir -p /srv/gitlab-runner/config

mkdir -p /srv/gitlab-runner/volumes/client

# register runner
docker run -d --rm --name gitlab-runner \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner:Z \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /srv/gitlab-runner/volumes/client:/certs/client \
  gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "$GITLAB_SERVER_URL" \
  --registration-token "$GITLAB_SERVER_REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image "$RUNNER_DOCKER_IMAGE" \
  --docker-volumes "/certs/client" \
  "$DOCKER_PRIVILEGED_FLAG" \
  --description "$HOSTNAME" \
  --tag-list "$GITLAB_RUNNER_TAG_LIST" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

if [ "$(docker container wait gitlab-runner)" != 0 ]; 
then
    echo "########################################################################################"
    echo "--------- gitlab-runner registration on $HOSTNAME failed. Aborting..."
    echo "########################################################################################"
    exit 1
fi

# run runner
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner:Z \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /srv/gitlab-runner/volumes/client:/certs/client \
  gitlab/gitlab-runner:latest run 

echo "########################################################################################"
echo "--------- Finished installing gitlab-runner on $HOSTNAME"
echo "########################################################################################"
