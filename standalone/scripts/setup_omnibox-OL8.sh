

install_gitlab() {

    firewall-offline-cmd --add-service=http
    firewall-offline-cmd --add-service=https
    systemctl reload firewalld
    
    dnf install -y curl
    #yum install -y curl
    #systemctl enable sshd
    #systemctl start sshd
    # sleep 30
    curl -O https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh 
    os=el dist=8 bash script.rpm.sh
    

    arch=$(uname -m)
    if [ "$arch" == "aarch64" ]; then
        #Cannot install latest release due to Redis issue: https://github.com/redis/redis/pull/8405
        dnf install -y gitlab-ee-13.8.6-ee.0.el8.aarch64
        #yum install -y gitlab-ee-13.8.6-ee.0.el8.aarch64
    else
        dnf install -y gitlab-ee
    fi
    
    if [ "$?" == "0" ]; then
        echo "Finished gitlab-ee installation"
    else
        echo "Failed to install gitlab-ee"
        exit 1
    fi
}


# setup_postfix() {

#     dnf install -y postfix

#     firewall-offline-cmd --add-service=smtp
#     systemctl reload firewalld

#     sudo dnf remove sendmail
#     sudo alternatives --set mta /usr/sbin/sendmail.postfix
    
#     systemctl enable --now postfix    
  
#     echo "gitlab_rails['smtp_enable'] = $smtp_enable" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_address'] = '$smtp_address'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_port'] = $smtp_port" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_user_name'] = '$smtp_user_name'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_password'] = '$smtp_password'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_domain'] = '$smtp_domain'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_authentication'] = '$smtp_authentication'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_enable_starttls_auto'] = $smtp_enable_starttls_auto" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_tls'] = $smtp_tls" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['smtp_openssl_verify_mode'] = '$smtp_openssl_verify_mode'" >> /etc/gitlab/gitlab.rb

#     echo "postfix mail installed and gitlab setup"
# }

# setup_object_storage() {
#     ## Object Storage
#     echo "gitlab_rails['object_store']['enabled'] = true" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['connection'] = {" >> /etc/gitlab/gitlab.rb
#     echo "  'provider' => 'AWS'," >> /etc/gitlab/gitlab.rb
#     echo "  'region' => '$home_region'," >> /etc/gitlab/gitlab.rb
#     echo "  'aws_access_key_id' => '$access_key'," >> /etc/gitlab/gitlab.rb
#     echo "  'aws_secret_access_key' => '$secret_key'," >> /etc/gitlab/gitlab.rb
#     echo "  'host' => '$namespace.compat.objectstorage.$home_region.oraclecloud.com'," >> /etc/gitlab/gitlab.rb
#     echo "  'path_style' => true," >> /etc/gitlab/gitlab.rb
#     echo "  'enable_signature_v4_streaming' => false" >> /etc/gitlab/gitlab.rb
#     echo "}" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['artifacts']['bucket'] = '$artifacts'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = '$external_diffs'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['lfs']['bucket'] = '$lfs'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['uploads']['bucket'] = '$uploads'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['packages']['bucket'] = '$packages'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = '$dependency_proxy'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = '$terraform_state'" >> /etc/gitlab/gitlab.rb

#     ## Backup setting to object storage
#     echo "gitlab_rails['manage_backup_path'] = true" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_path'] = '/var/opt/gitlab/backups'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_archive_permissions'] = 0644" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_pg_schema'] = 'public'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_keep_time'] = 604800" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_upload_connection'] = {" >> /etc/gitlab/gitlab.rb
#     echo "  'provider' => 'AWS'," >> /etc/gitlab/gitlab.rb
#     echo "  'region' => '$home_region'," >> /etc/gitlab/gitlab.rb
#     echo "  'aws_access_key_id' => '$access_key'," >> /etc/gitlab/gitlab.rb
#     echo "  'aws_secret_access_key' => '$secret_key'," >> /etc/gitlab/gitlab.rb
#     echo "  'host' => '$namespace.compat.objectstorage.$home_region.oraclecloud.com'," >> /etc/gitlab/gitlab.rb
#     echo "  'path_style' => true," >> /etc/gitlab/gitlab.rb
#     echo "  'enable_signature_v4_streaming' => false" >> /etc/gitlab/gitlab.rb
#     echo "}" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_upload_remote_directory'] = '$backup_upload_remote_directory'" >> /etc/gitlab/gitlab.rb
#     echo "gitlab_rails['backup_multipart_chunk_size'] = 104857600" >> /etc/gitlab/gitlab.rb
# }


initial_root_password() {
    echo "gitlab_rails['initial_root_password'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
 
    ## Gitaly and GitLab use two shared secrets for authentication
    #echo "gitlab_rails['gitaly_token'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    #echo "gitlab_shell['secret_token'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
}

setup_external_url(){

    sed -i"" "s#external_url 'http://gitlab.example.com'##g" /etc/gitlab/gitlab.rb
    echo "external_url '$external_url'" >> /etc/gitlab/gitlab.rb

}

install_gitlab

if [ "$IS_EXTERNAL_URL" == 0 ]; then
  export RUN_RECONFIGURE=1
fi

# if [ "$smtp_enable" == true ]; then
#     setup_postfix
#     export RUN_RECONFIGURE=1
# fi
# elif [ "$IS_REDIS_SERVER" == 1 ]; then
#   setup_redis_config
# elif [ "$IS_GITALY_SERVER" == 1 ]; then
#   setup_gitaly_config
# elif [ "$IS_MONITORING_SERVER" == 1 ]; then
#   setup_monitoring_config
if [ "$RUN_RECONFIGURE" == 1 ]; then
    echo "runing gitlab-ctl reconfigure"
    gitlab-ctl reconfigure
fi

#initial_root_password


