export dns_hostname=

install_postfix() {    
    debconf-set-selections <<< "postfix postfix/mailname string $${dns_hostname}"
    debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
    apt-get install --assume-yes postfix
    echo "postfix mail installed"
}

install_gitlab() {
    #apt-get update is required in order to install jq
    apt-get update
    #required packages already installed on platform image but trying to install just in case it changes.
    apt-get install -y curl openssh-server ca-certificates tzdata jq

    curl -vvv https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
    apt-get install -y gitlab-ee
    
    echo "Finished gitlab-ee installation"
}

initial_root_password() {
    export INSTANCE_OCID=$(curl -H "Authorization: Bearer Oracle" http://169.254.169.254/opc/v2/instance/ | jq -r .id)
    sed -i"" "s#\# gitlab_rails\['initial_root_password.*#gitlab_rails\['initial_root_password'\] = \""$INSTANCE_OCID"\"#g" /etc/gitlab/gitlab.rb

    echo "Initial GitLab server root password defined"
}

update_iptables() {
    echo "updatingiptables"
    iptables -I INPUT 6 -m state --state NEW -p tcp --dport 80 -j ACCEPT
    iptables -I INPUT 6 -m state --state NEW -p tcp --dport 443 -j ACCEPT
    netfilter-persistent save
    echo "Finished updatingiptables"
}

gitlab_reconfigure() {
    gitlab-ctl reconfigure
}


update_iptables
#install_postfix
install_gitlab
initial_root_password
gitlab_reconfigure

