
###
# compute.tf outputs
###

output "instance_id" {
  value = oci_core_instance.simple-vm.id
}

output "instance_public_ip" {
  value = oci_core_instance.simple-vm.public_ip
}

output "instance_private_ip" {
  value = oci_core_instance.simple-vm.private_ip
}

// output "instance_https_url" {
//   value = (local.is_public_subnet ? "https://${oci_core_instance.simple-vm.public_ip}" : "https://${oci_core_instance.simple-vm.private_ip}")
// }

output "server_url" {
  value = var.external_url != "" ? var.external_url : (local.is_public_subnet ? "http://${oci_core_instance.simple-vm.public_ip}" : "http://${oci_core_instance.simple-vm.private_ip}")
}

output "external_url" {
  value = var.external_url
}

output "vcn_cidr_block" {
  value = !local.use_existing_network ? join("", oci_core_vcn.simple.*.cidr_block) : var.vcn_cidr_block
}


###
# GitLab Server outputs
###

// output "initial_root_password" {
//   value = random_password.password.result
// }

output "message" {
  value = "It may take some minutes to complete GitLab installation. You can SSH to the instance and check log messages through `sudo cat /var/log/messages` or `sudo tail -f /var/log/messages` [Ctrl+C to exit]\n. If you set up an external_url, modify your DNS records on your DNS provider."
}